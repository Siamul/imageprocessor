Implementation of all the functionalities are done in the file "ImageViewer.java." The "ImageProcessor.java" just contains the main function and calls ImageViewer. The convolution/correlation operation is done using a double loop and iterating the filter over the image. I know that this is an inefficient implementation and using a matrix library I can speed up the computation but such an implementation is easier for students to understand.
I plan to modularize the code, putting each image processing procedure in its own file, later.

The list below shows the functionalities of the application.

    Basic image modifications: grayscale, negative, mirror, rotate.
    Log Transformation and its inverse.
    Histogram Equalization.
    Image Blurring.
    Sharpening using Laplacian filters.
    Resizing: Nearest Neighbor and Bilinear techiques.
    Thresholding: Manual, Iterative and Otsu's Method.