/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageprocessor;

import java.awt.Color;
import java.awt.List;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import javax.imageio.*;
import javax.swing.*;

/**
 *
 * @author SiamulKarim
 */
public class ImageViewer extends JFrame {
    JLabel label;
    JFileChooser choice;
    JMenu menu;
    JMenuBar menuBar;
    JMenuItem menuItem;
    JMenuItem menuItem2;
    JMenuItem menuItem3;
    JMenuItem menuItem4;
    JMenuItem menuItem5;
    JMenuItem menuItem6;
    JMenuItem menuItem7;
    JMenuItem menuItem8;
    JMenuItem menuItem9;    
    JMenuItem menuItem10;
    JMenuItem menuItem11;
    JMenuItem menuItem12;
    JMenuItem menuItem13;
    JMenuItem menuItem14;
    JMenuItem menuItem15;
    JMenuItem menuItem16;
    JMenuItem menuItem17;
    JMenuItem menuItem18;
    JMenuItem menuItem19;
    JMenuItem menuItem20;
    JMenuItem menuItem21;
    BufferedImage image = null;
    int width;
    int height;
    String fileName;
            
    public double[][][] correlation(BufferedImage origImg, double[][] filter)
    {
        int fcenter = (filter.length-1)/2;
        int width = origImg.getWidth();
        int height = origImg.getHeight();
        double[][][] retArr = new double[3][height][width];
        //BufferedImage retImg = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        for(int i=0; i<height; i++)
        {         
            for(int j=0; j<width; j++)
            {
                int f_a = 0;
                double r_sum = 0;
                double g_sum = 0;
                double b_sum = 0;
                for(int a = i-fcenter; a<=i+fcenter; a++)
                {
                    if(a<0 || a>=height) continue;
                    int f_b = 0;
                    for(int b = j-fcenter; b<=j+fcenter; b++)
                    {
                        if(b<0 || b>=width) continue;
                        Color c = new Color(origImg.getRGB(b, a));
                        r_sum += filter[f_a][f_b]*c.getRed();
                        g_sum += filter[f_a][f_b]*c.getGreen();
                        b_sum += filter[f_a][f_b]*c.getBlue();
                        f_b++;
                    }
                    f_a++;
                }
                retArr[0][i][j] = r_sum;
                retArr[1][i][j] = g_sum;
                retArr[2][i][j] = b_sum;
                //Color newColor = new Color((int)r_sum, (int)g_sum, (int)b_sum);
                //retImg.setRGB(j,i,newColor.getRGB());
            }
        }
        return retArr;        
    }
    
    public boolean[][] thresholdImage(BufferedImage origImg, int threshold)
    {
        int width = origImg.getWidth();
        int height = origImg.getHeight();
        boolean[][] retArr = new boolean[height][width];
        //BufferedImage retImg = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        for(int i=0; i<height; i++)
        {         
            for(int j=0; j<width; j++)
            {
                Color c = new Color(origImg.getRGB(j, i));
                int red = (int)(c.getRed() * 0.299);
                int green = (int)(c.getGreen() * 0.587);
                int blue = (int)(c.getBlue() *0.114);
                int grayValue = red+green+blue;
                if(grayValue > threshold) 
                {
                    retArr[i][j] = true;
                }
                else
                {
                    retArr[i][j] = false;
                }
            }
        }
        return retArr;        
    }
    
    public ImageViewer()
    {
        setExtendedState(JFrame.MAXIMIZED_BOTH); 
        label = new JLabel();
        add(label);
        
        choice = new JFileChooser();
        choice.setCurrentDirectory(new File("."));
        
        menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        
        menu = new JMenu("File");
        menuBar.add(menu);
        
        menuItem = new JMenuItem("Open");
        menu.add(menuItem);
        
        menuItem2 = new JMenuItem("Original");
        menu.add(menuItem2);
        
        menuItem3 = new JMenuItem("Grayscale");
        menu.add(menuItem3);
        
        menuItem4 = new JMenuItem("Image Negative");
        menu.add(menuItem4);
        
        menuItem5 = new JMenuItem("Grayscale Image Negative");
        menu.add(menuItem5);
        
        menuItem6 = new JMenuItem("Mirror Vertical");
        menu.add(menuItem6);
        
        menuItem7 = new JMenuItem("Mirror Horizontal");
        menu.add(menuItem7);
        
        menuItem8 = new JMenuItem("Rotate 90 degree clockwise");
        menu.add(menuItem8);
        
        menuItem9 = new JMenuItem("Rotate 90 degree anticlockwise");
        menu.add(menuItem9);
        
        menuItem10 = new JMenuItem("Rotate 180 degree");
        menu.add(menuItem10);
        
        menuItem11 = new JMenuItem("Log Transform");
        menu.add(menuItem11);
        
        menuItem12 = new JMenuItem("Inverse Log Transform");
        menu.add(menuItem12);
        
        menuItem13 = new JMenuItem("Histogram Equalization");
        menu.add(menuItem13);
        
        menuItem14 = new JMenuItem("Blur");
        menu.add(menuItem14);
        
        menuItem15 = new JMenuItem("Sharpen with Laplacian");
        menu.add(menuItem15);
        
        menuItem16 = new JMenuItem("Sharpen with Diagonal-included Laplacian");
        menu.add(menuItem16);
        
        menuItem17 = new JMenuItem("Nearest Neighbor Resizing");
        menu.add(menuItem17);
        
        menuItem18 = new JMenuItem("Bilinear Resizing");
        menu.add(menuItem18);
        
        menuItem19 = new JMenuItem("Manual Thresholding");
        menu.add(menuItem19);
        
        menuItem20 = new JMenuItem("Iterative Thresholding");
        menu.add(menuItem20);
        
        menuItem21 = new JMenuItem("Otsu Thresholding");
        menu.add(menuItem21);
        
        menuItem.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                int result = choice.showOpenDialog(null);
                if(result == JFileChooser.APPROVE_OPTION)
                {
                    fileName = choice.getSelectedFile().getPath();
                    label.setIcon(new ImageIcon(fileName));
                }
                try
                {
                    File input = new File(fileName);
                    image = ImageIO.read(input);
                    width = image.getWidth();
                    height = image.getHeight();
                    /*for(int i=0; i<height; i++){
                        for(int j=0; j<width; j++){
                           Color c = new Color(image.getRGB(j, i));
                           System.out.println(" Red: " + c.getRed() +"  Green: " + c.getGreen() + " Blue: " + c.getBlue());
                        }
                     }*/
                }
                catch (Exception e)
                {
                    System.out.println(e);
                }
            }
        });
        
        menuItem2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                label.setIcon(new ImageIcon(fileName));
            }
        });
        
        menuItem3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                try{
                    for(int i=0; i<height; i++)
                    {         
                        for(int j=0; j<width; j++)
                        {
                           Color c = new Color(image.getRGB(j, i));
                           int red = (int)(c.getRed() * 0.299);
                           int green = (int)(c.getGreen() * 0.587);
                           int blue = (int)(c.getBlue() *0.114);
                           int grayValue = red+green+blue;
                           Color newColor = new Color(grayValue,grayValue,grayValue);
                           tempImage.setRGB(j,i,newColor.getRGB());
                        }
                     }

                     File output = new File("grayscale.png");
                     ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("grayscale.png"));
            }
        });
        
        menuItem4.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                try{
                    for(int i=0; i<height; i++)
                    {         
                        for(int j=0; j<width; j++)
                        {
                           Color c = new Color(image.getRGB(j, i));
                           int red = (int)(255 - c.getRed());
                           int green = (int)(255 - c.getGreen());
                           int blue = (int)(255 - c.getBlue());
                           Color newColor = new Color(red,green,blue);
                           tempImage.setRGB(j,i,newColor.getRGB());
                        }
                     }

                     File output = new File("negative.png");
                     ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("negative.png"));
            }
        });
        
        menuItem5.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                try{
                    for(int i=0; i<height; i++)
                    {         
                        for(int j=0; j<width; j++)
                        {
                           Color c = new Color(image.getRGB(j, i));
                           int red = (int)(c.getRed() * 0.299);
                           int green = (int)(c.getGreen() * 0.587);
                           int blue = (int)(c.getBlue() *0.114);
                           int grayValue = red+green+blue;
                           Color newColor = new Color(255-grayValue,255-grayValue,255-grayValue);
                           tempImage.setRGB(j,i,newColor.getRGB());
                        }
                     }

                     File output = new File("grayscalenegative.png");
                     ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("grayscalenegative.png"));
            }
        });
        menuItem6.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                try{
                    for(int i=0; i<height; i++)
                    {         
                        for(int j=0; j<width; j++)
                        {
                           tempImage.setRGB(width-1-j,i,image.getRGB(j,i));
                        }
                     }

                     File output = new File("mirrorvertical.png");
                     ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("mirrorvertical.png"));
            }
        });
        menuItem7.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                try{
                    for(int i=0; i<height; i++)
                    {         
                        for(int j=0; j<width; j++)
                        {
                           tempImage.setRGB(j,height-1-i,image.getRGB(j,i));
                        }
                     }

                     File output = new File("mirrorhorizontal.png");
                     ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("mirrorhorizontal.png"));
            }
        });
        
        menuItem8.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(height,width,BufferedImage.TYPE_INT_RGB);
                try{
                    for(int i=0; i<width; i++)
                    {         
                        for(int j=0; j<height; j++)
                        {
                           tempImage.setRGB(j,i,image.getRGB(i,height-1-j));
                        }
                     }

                     File output = new File("rotate90clockwise.png");
                     ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("rotate90clockwise.png"));
            }
        });
        
        menuItem9.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(height,width,BufferedImage.TYPE_INT_RGB);
                try{
                    for(int i=0; i<width; i++)
                    {         
                        for(int j=0; j<height; j++)
                        {
                           tempImage.setRGB(j,i,image.getRGB(width-1-i,j));
                        }
                     }

                     File output = new File("rotate90anticlockwise.png");
                     ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("rotate90anticlockwise.png"));
            }
        });
        
        menuItem10.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                try{
                    for(int i=0; i<height; i++)
                    {         
                        for(int j=0; j<width; j++)
                        {
                           tempImage.setRGB(j,i,image.getRGB(width-1-j,height-1-i));
                        }
                     }

                     File output = new File("rotate180.png");
                     ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("rotate180.png"));
            }
        });
        
        menuItem11.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                try{
                    for(int i=0; i<height; i++)
                    {         
                        for(int j=0; j<width; j++)
                        {
                           Color c = new Color(image.getRGB(j, i));
                           int red = (int)(c.getRed() * 0.299);
                           int green = (int)(c.getGreen() * 0.587);
                           int blue = (int)(c.getBlue() *0.114);
                           int grayValue = red+green+blue;
                           int logValue = (int) ((255/Math.log10(256)) * Math.log10(1+grayValue));
                           if(logValue > 255 || logValue < 0) System.out.println("O Hell No!" + logValue);
                           Color newColor = new Color(logValue,logValue,logValue);
                           tempImage.setRGB(j,i,newColor.getRGB());
                        }
                     }

                     File output = new File("logtransform.png");
                     ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("logtransform.png"));
            }
        });
        
        menuItem12.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                try{
                    for(int i=0; i<height; i++)
                    {         
                        for(int j=0; j<width; j++)
                        {
                           Color c = new Color(image.getRGB(j, i));
                           int red = (int)(c.getRed() * 0.299);
                           int green = (int)(c.getGreen() * 0.587);
                           int blue = (int)(c.getBlue() *0.114);
                           int grayValue = red+green+blue;
                           int logValue = (int) (Math.pow(10, (grayValue*Math.log10(256))/255) - 1);
                           if(logValue > 255 || logValue < 0) System.out.println("O Hell No! " + logValue);
                           Color newColor = new Color(logValue,logValue,logValue);
                           tempImage.setRGB(j,i,newColor.getRGB());
                        }
                     }

                     File output = new File("invlogtransform.png");
                     ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("invlogtransform.png"));
            }
        });
        
        menuItem13.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                BufferedImage tempImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                try{
                    double[] freq = new double[256];
                    double[] cfreq = new double[256];
                    int[] intMap = new int[256];
                    int[][] grayImage = new int[height][width];
                    for(int i=0; i<height; i++)
                    {         
                        for(int j=0; j<width; j++)
                        {
                            Color c = new Color(image.getRGB(j, i));
                            int red = (int)(c.getRed() * 0.299);
                            int green = (int)(c.getGreen() * 0.587);
                            int blue = (int)(c.getBlue() *0.114);
                            int grayValue = red+green+blue;
                            grayImage[i][j] = grayValue;
                            freq[grayValue] = freq[grayValue] + 1;
                        }
                    }
                    double sum = 0;
                    for(int i = 0; i<256; i++)
                    {
                        sum = sum + freq[i];
                        cfreq[i] = sum;
                    }
                    for(int i = 0; i<256; i++)
                    {
                        //System.out.println(i + "->" + Math.floor((cfreq[i]/(height*width))*255));
                        intMap[i] = (int)Math.floor((cfreq[i]/(height*width))*255);
                        //System.out.println(i + "->" + intMap[i]);
                    }
                    for(int i=0; i<height; i++)
                    {         
                        for(int j=0; j<width; j++)
                        {
                            Color newColor = new Color(intMap[grayImage[i][j]],intMap[grayImage[i][j]],intMap[grayImage[i][j]]);
                            tempImage.setRGB(j,i,newColor.getRGB());
                        }
                    }
                    File output = new File("histogramequalized.png");
                    ImageIO.write(tempImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("histogramequalized.png"));
            }
        });
        
        menuItem14.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                int flength = 9;
                double[][] filter = new double[flength][flength];
                for(int i = 0; i<flength; i++)
                {
                    for(int j = 0; j<flength; j++)
                    {
                        filter[i][j] = (1.0/(double)(flength*flength));
                    }
                }
                BufferedImage blurImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                double[][][] blurArr = correlation(image,filter);
                for(int i=0; i<height; i++)
                {         
                    for(int j=0; j<width; j++)
                    {
                        Color newColor = new Color((int)blurArr[0][i][j],(int)blurArr[1][i][j],(int)blurArr[2][i][j]);
                        blurImage.setRGB(j,i,newColor.getRGB());
                    }
                }
                try{
                    File output = new File("blurred.png");
                    ImageIO.write(blurImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("blurred.png"));
               
            }
        });
        menuItem15.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                double[][] filter = {
                    {0,1,0},
                    {1,-4,1},
                    {0,1,0}
                };
                BufferedImage sharpImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                double[][][] laplacianArr = correlation(image,filter);
                for(int i=0; i<height; i++)
                {         
                    for(int j=0; j<width; j++)
                    {
                        Color c = new Color(image.getRGB(j, i));
                        int red = (int)(c.getRed()-laplacianArr[0][i][j]);
                        if(red < 0) red = 0;
                        else if(red > 255) red = 255;
                        int green = (int)(c.getGreen()-laplacianArr[1][i][j]);
                        if(green < 0) green = 0;
                        else if(green > 255) green = 255;
                        int blue = (int)(c.getBlue()-laplacianArr[2][i][j]);
                        if(blue < 0) blue = 0;
                        else if(blue > 255) blue = 255;
                        Color newColor = new Color(red, green, blue);
                        sharpImage.setRGB(j,i,newColor.getRGB());
                    }
                }
                 try{
                    File output = new File("sharp.png");
                    ImageIO.write(sharpImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("sharp.png"));
            }
        });
        
        menuItem16.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                double[][] filter = {
                    {1,1,1},
                    {1,-8,1},
                    {1,1,1}
                };
                BufferedImage sharpImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                double[][][] laplacianArr = correlation(image,filter);
                for(int i=0; i<height; i++)
                {         
                    for(int j=0; j<width; j++)
                    {
                        Color c = new Color(image.getRGB(j, i));
                        int red = (int)(c.getRed()-laplacianArr[0][i][j]);
                        if(red < 0) red = 0;
                        else if(red > 255) red = 255;
                        int green = (int)(c.getGreen()-laplacianArr[1][i][j]);
                        if(green < 0) green = 0;
                        else if(green > 255) green = 255;
                        int blue = (int)(c.getBlue()-laplacianArr[2][i][j]);
                        if(blue < 0) blue = 0;
                        else if(blue > 255) blue = 255;
                        Color newColor = new Color(red, green, blue);
                        sharpImage.setRGB(j,i,newColor.getRGB());
                    }
                }
                 try{
                    File output = new File("sharpDiag.png");
                    ImageIO.write(sharpImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("sharpDiag.png"));
            }
        });
        
         menuItem17.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                int newHeight, newWidth;
                JTextField heightBox = new JTextField(10);
                JTextField widthBox = new JTextField(10);
                JPanel dialogBox = new JPanel();
                dialogBox.add(new JLabel("width: "));
                dialogBox.add(widthBox);
                dialogBox.add(new JLabel("height: "));
                dialogBox.add(heightBox);
                int selection = JOptionPane.showConfirmDialog(null, dialogBox, 
                        "Enter new dimensions for the image : ", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if(selection == JOptionPane.OK_OPTION)
                {
                    newHeight = Integer.parseInt(heightBox.getText());
                    newWidth = Integer.parseInt(widthBox.getText());
                }
                else
                {
                    return;
                }
                //System.out.println(newHeight + " , " + newWidth);
                double h = ((double)(height - 1))/((double)(newHeight - 1));
                double w = ((double)(width - 1))/((double)(newWidth - 1));
                BufferedImage scaledImage = new BufferedImage(newWidth,newHeight,BufferedImage.TYPE_INT_RGB);
                for(int i=0; i<newHeight; i++)
                {         
                    for(int j=0; j<newWidth; j++)
                    {
                       scaledImage.setRGB(j,i,image.getRGB((int) Math.round(w*j),(int) Math.round(h*i)));
                    }
                }
                try{
                    File output = new File("nearestNeighbor.png");
                    ImageIO.write(scaledImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("nearestNeighbor.png"));               
                        
            }
        });
         
        menuItem18.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                int newHeight, newWidth;
                JTextField heightBox = new JTextField(10);
                JTextField widthBox = new JTextField(10);
                JPanel dialogBox = new JPanel();
                dialogBox.add(new JLabel("width: "));
                dialogBox.add(widthBox);
                dialogBox.add(new JLabel("height: "));
                dialogBox.add(heightBox);
                int selection = JOptionPane.showConfirmDialog(null, dialogBox, 
                        "Enter new dimensions for the image : ", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if(selection == JOptionPane.OK_OPTION)
                {
                    newHeight = Integer.parseInt(heightBox.getText());
                    newWidth = Integer.parseInt(widthBox.getText());
                }
                else
                {
                    return;
                }
                //System.out.println(newHeight + " , " + newWidth);
                double h = ((double)(height - 1))/((double)(newHeight - 1));
                double w = ((double)(width - 1))/((double)(newWidth - 1));
                BufferedImage scaledImage = new BufferedImage(newWidth,newHeight,BufferedImage.TYPE_INT_RGB);
                for(int i=0; i<newHeight; i++)
                {         
                    for(int j=0; j<newWidth; j++)
                    {
                       Color[][] color = new Color[2][2];
                       int y0 = (int) Math.floor(w*(double)j);
                       y0 = (y0 > 0) ? y0 : 0;
                       int y1 = (int) Math.ceil(w*(double)j);
                       y1 = (y1 < width) ? y1 : width - 1;
                       //if(y1 > width) System.out.println("y1: " + y1);
                       int x0 = (int) Math.floor(h*(double)i);
                       x0 = (x0 > 0) ? x0 : 0;
                       int x1 = (int) Math.ceil(h*(double)i);
                       //if(x1 > height) System.out.println("x1: " + x1);
                       x1 = (x1 < height) ? x1 : height - 1;
                       
                       color[0][0] = new Color(image.getRGB(y0, x0));
                       color[0][1] = new Color(image.getRGB(y1, x0));
                       color[1][0] = new Color(image.getRGB(y0, x1));
                       color[1][1] = new Color(image.getRGB(y1, x1));
                       double r00 = (double)color[0][0].getRed();
                       double r01 = (double)color[0][1].getRed();
                       double r10 = (double)color[1][0].getRed();
                       double r11 = (double)color[1][1].getRed();
                       double g00 = (double)color[0][0].getGreen();
                       double g01 = (double)color[0][1].getGreen();
                       double g10 = (double)color[1][0].getGreen();
                       double g11 = (double)color[1][1].getGreen();
                       double b00 = (double)color[0][0].getBlue();
                       double b01 = (double)color[0][1].getBlue();
                       double b10 = (double)color[1][0].getBlue();
                       double b11 = (double)color[1][1].getBlue();
                       double d0 = (x0 - x1)*(y0 - y1);
                       double d1 = -d0;
                       
                       double a0_red = (r00*x1*y1)/d0 + (r01*x1*y0)/d1 + (r10*x0*y1)/d1 + (r11*x0*y0)/d0;
                       double a0_green = (g00*x1*y1)/d0 + (g01*x1*y0)/d1 + (g10*x0*y1)/d1 + (g11*x0*y0)/d0;
                       double a0_blue = (b00*x1*y1)/d0 + (b01*x1*y0)/d1 + (b10*x0*y1)/d1 + (b11*x0*y0)/d0;
                       double a1_red = (r00*y1)/d1 + (r01*y0)/d0 + (r10*y1)/d0 + (r11*y0)/d1;
                       double a1_green = (g00*y1)/d1 + (g01*y0)/d0 + (g10*y1)/d0 + (g11*y0)/d1;
                       double a1_blue = (b00*y1)/d1 + (b01*y0)/d0 + (b10*y1)/d0 + (b11*y0)/d1;
                       double a2_red = (r00*x1)/d1 + (r01*x1)/d0 + (r10*x0)/d0 + (r11*x0)/d1;
                       double a2_green = (g00*x1)/d1 + (g01*x1)/d0 + (g10*x0)/d0 + (g11*x0)/d1;
                       double a2_blue = (b00*x1)/d1 + (b01*x1)/d0 + (b10*x0)/d0 + (b11*x0)/d1;
                       double a3_red = r00/d0 + r01/d1 + r10/d1 + r11/d0;
                       double a3_green = g00/d0 + g01/d1 + g10/d1 + g11/d0;
                       double a3_blue = b00/d0 + b01/d1 + b10/d1 + b11/d0;
                       double x = h*(double)i;
                       double y = w*(double)j;
                       //System.out.println(x + ", " + y);
                       double red = a0_red + a1_red*x + a2_red*y + a3_red*x*y;
                       double green = a1_green*x + a2_green*y + a3_green*x*y + a0_green;
                       double blue = a1_blue*x + a2_blue*y + a3_blue*x*y + a0_blue;
                       //if(red > 255 || green > 255 || blue > 255) System.out.println(red + ", " +green+ ", "+blue);
                       Color newColor = new Color((int) Math.round(red), (int) Math.round(green), (int) Math.round(blue));
                       scaledImage.setRGB(j,i,newColor.getRGB());
                    }
                }
                try{
                    File output = new File("bilinear.png");
                    ImageIO.write(scaledImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("bilinear.png"));               
                        
            }
        });
        
        menuItem19.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                int thresholdValue;
                JTextField thresholdBox = new JTextField(10);
                JPanel dialogBox = new JPanel();
                dialogBox.add(new JLabel("Threshold Value: "));
                dialogBox.add(thresholdBox);
                int selection = JOptionPane.showConfirmDialog(null, dialogBox, 
                        "Enter value for image thresholding: ", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if(selection == JOptionPane.OK_OPTION)
                {
                    thresholdValue = Integer.parseInt(thresholdBox.getText());
                }
                else
                {
                    return;
                }
                BufferedImage thresholdedImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                boolean[][] threshold = thresholdImage(image, thresholdValue);
                for(int i = 0 ; i< threshold.length; i++)
                {
                    for(int j = 0; j < threshold[i].length; j++)      
                    {
                        Color newColor = threshold[i][j] ? new Color(255,255,255) : new Color(0,0,0);
                        thresholdedImage.setRGB(j, i, newColor.getRGB());    
                    }
                }
                try{
                    File output = new File("manualThreshold.png");
                    ImageIO.write(thresholdedImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("manualThreshold.png"));               
                        
            }
        });
        menuItem20.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                int thresholdValue = 0;
                double[] freq = new double[256];
                int[][] grayImage = new int[height][width];
                for(int i=0; i<height; i++)
                {         
                    for(int j=0; j<width; j++)
                    {
                        Color c = new Color(image.getRGB(j, i));
                        int red = (int)(c.getRed() * 0.299);
                        int green = (int)(c.getGreen() * 0.587);
                        int blue = (int)(c.getBlue() *0.114);
                        int grayValue = red+green+blue;
                        grayImage[i][j] = grayValue;
                        freq[grayValue] = freq[grayValue] + 1;
                    }
                }
                double sum = 0;
                double T = 0;
                for(int i = 0; i<256; i++)
                {
                    sum += freq[i];
                    T += i*freq[i];
                }
                T /= sum;
                double delT = Double.POSITIVE_INFINITY;
                while(delT > 0.01)
                {
                    double m1 = 0, m2 = 0, sum1 = 0, sum2 = 0;
                    for(int i = 0; i<T; i++)
                    {
                        m2 += i*freq[i];
                        sum2 += freq[i];
                    }
                    m2 /= sum2;
                    for(int i = (int)Math.ceil(T); i<256; i++)
                    {
                        m1 += i*freq[i];
                        sum1 += freq[i];
                    }
                    m1 /= sum1;
                    delT = Math.abs(T - 0.5*(m1+m2));
                    T = 0.5*(m1+m2);
                    System.out.println("Del T: " + delT);
                }
                System.out.println("Iterative K: " + T);
                thresholdValue = (int)Math.floor(T);
                BufferedImage iterThresImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                boolean[][] threshold = thresholdImage(image, thresholdValue);
                for(int i = 0 ; i< threshold.length; i++)
                {
                    for(int j = 0; j < threshold[i].length; j++)      
                    {
                        Color newColor = threshold[i][j] ? new Color(255,255,255) : new Color(0,0,0);
                        iterThresImage.setRGB(j, i, newColor.getRGB());    
                    }
                }
                try{
                    File output = new File("iterativeThreshold.png");
                    ImageIO.write(iterThresImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("iterativeThreshold.png"));               
                        
            }
        });
        menuItem21.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event)
            {
                if(image == null) return;
                int thresholdValue = 0;
                double[] freq = new double[256];
                int[][] grayImage = new int[height][width];
                for(int i=0; i<height; i++)
                {         
                    for(int j=0; j<width; j++)
                    {
                        Color c = new Color(image.getRGB(j, i));
                        int red = (int)(c.getRed() * 0.299);
                        int green = (int)(c.getGreen() * 0.587);
                        int blue = (int)(c.getBlue() *0.114);
                        int grayValue = red+green+blue;
                        grayImage[i][j] = grayValue;
                        freq[grayValue] = freq[grayValue] + 1;
                    }
                }
                double sum = 0;
                double mG = 0;
                for(int i = 0; i<256; i++)
                {
                    sum += freq[i];
                    mG += i*freq[i];
                    freq[i] /= (width*height);
                }
                mG /= sum;
                double[] cSum = new double[256];
                double[] cMean = new double[256];
                double cSumTemp = 0, cMeanTemp = 0;
                for(int i = 0; i<256; i++)
                {
                    cSumTemp += freq[i];
                    cSum[i] = cSumTemp;
                    cMeanTemp += i*freq[i];
                    cMean[i] = cMeanTemp;
                }
                double[] bcvariance = new double[256];
                for(int i = 0; i<256; i++)
                {
                    double numerator = Math.pow((mG*cSum[i]-cMean[i]),2);
                    double denominator = cSum[i]*(1-cSum[i]);
                    bcvariance[i] = numerator/denominator;
                }
                ArrayList<Integer> maxIndex = new ArrayList();
                double maxVar = bcvariance[0];
                for(int i = 1; i<256; i++)
                {
                    if(maxVar < bcvariance[i])
                    {
                        maxVar = bcvariance[i];
                        maxIndex.clear();
                        maxIndex.add(i);
                    }
                    else if(maxVar == bcvariance[i])
                    {
                        maxIndex.add(i);
                    }
                }
                double optk = 0;
                for(int i = 0; i<maxIndex.size(); i++)
                {
                    optk += maxIndex.get(i);
                }
                optk /= maxIndex.size();
                thresholdValue = (int)Math.floor(optk);
                System.out.println("Optimal K: " + optk);
                BufferedImage iterThresImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
                boolean[][] threshold = thresholdImage(image, thresholdValue);
                for(int i = 0 ; i< threshold.length; i++)
                {
                    for(int j = 0; j < threshold[i].length; j++)      
                    {
                        Color newColor = threshold[i][j] ? new Color(255,255,255) : new Color(0,0,0);
                        iterThresImage.setRGB(j, i, newColor.getRGB());    
                    }
                }
                try{
                    File output = new File("otsuThreshold.png");
                    ImageIO.write(iterThresImage, "png", output);
                } catch (Exception e){
                    System.out.println(e);
                }
                label.setIcon(new ImageIcon("otsuThreshold.png"));               
                        
            }
        });
    }
}
